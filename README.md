CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Theory is a responsive theme with a crisp, modern landing page template ideal for businesses and corporations.

 * For a full description of the theme, visit the project page:
   https://www.drupal.org/project/theory

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/theory


REQUIREMENTS
------------

This theme require the following theme:

 * Bootstrap (https://www.drupal.org/project/bootstrap)


INSTALLATION
------------

As this theme is based on bootstrap you should download and enable
Drupal Bootstrap project.

Steps for smooth installation of theory theme:
  - Theme file can be downloaded from the link
    https://www.drupal.org/project/theory - Extract the downloaded
    file to the themes directory.

  - Goto Admin > Appearance, find theory theme and choose 'Install and
    set as default' option.

  - You have now enabled your theme.


CONFIGURATION
-------------

The theme sections can be customized from the theme settings in
admin area.


MAINTAINERS
-----------

Current maintainer:
 * zyxware - https://www.drupal.org/u/zyxware
